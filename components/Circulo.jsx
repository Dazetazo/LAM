import React from 'react';
import Image from "next/image";

const Circulo = ({imagen, descripcion = ''}) => {
    return (
        <div className={'flex flex-col mx-auto mt-8 w-36 h-48 justify-between'}>
            <Image src={imagen} className={'text-center items-center rounded-circle'} width={110} height={140}/>
            <h6 className={'text-center font-bold rounded justify-center items-center'}>{descripcion}</h6>
        </div>
    );
};

export default Circulo;