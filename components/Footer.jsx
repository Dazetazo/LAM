import React from 'react';
import Link from "next/link";
import { Container, Nav } from "react-bootstrap";
import Image from "next/image";
import { BsFacebook, BsGithub, BsInstagram, BsLinkedin } from "react-icons/bs"


const Footer = () => {
    return (
        <footer className={'bg-dark p-2 mt-5'}>
            <Container>
                <div className={'flex items-center justify-between flex-col md:flex-row'}>
                    <div className={'flex items-center flex-col'}>
                        <Image src={'/LAM.jpg'} width={200} height={150}/>
                        <Nav className={'flex flex-row items-center justify-center gap-4 mb-2'}>
                            <Link href={'/'}>
                                <a className={'text-white text-lg no-underline font-bold'}>
                                    Home
                                </a>
                            </Link>
                            <Link href={'/proyectos'}>
                                <a className={'text-white text-lg no-underline font-bold'}>
                                    Proyectos
                                </a>
                            </Link>
                            <Link href={'/nosotros'}>
                                <a className={'text-white text-lg no-underline font-bold'}>
                                    Nosotros
                                </a>
                            </Link>
                        </Nav>
                        <p className={'text-gray-500 font-bold text-sm text-center'}>Todos los derechos reservados © {new Date().getFullYear()}</p>
                    </div>
                    <div className={'flex items-center flex-col justify-center'}>
                        <p className={'text-white font-medium text-lg'}>Maricones Ricos</p>
                        <p className={'text-white font-medium text-lg'}>+569 66666666</p>
                        <p className={'text-white font-medium text-lg'}>lam@gmail.com</p>
                    </div>
                    <div className={'flex items-center justify-center md:mt-0 mt-10'}>
                        <BsFacebook className={'text-gray-500 text-4xl m-2'}/>
                        <BsGithub className={'text-gray-500 text-4xl m-2'}/>
                        <BsInstagram className={'text-gray-500 text-4xl m-2'}/>
                        <BsLinkedin className={'text-gray-500 text-4xl m-2'}/>
                    </div>
                </div>
            </Container>
        </footer>
    );
};

export default Footer;