import React from 'react';
import { Button, Carousel, Container, Navbar } from "react-bootstrap";
import Link from "next/link";
import {useRouter} from "next/router";

const Header = () => {

    const router = useRouter()

    return (
        <>
            <Navbar className="navbar navbar-light navbar-expand bg-dark">
                <Container>
                    <Link href="#">
                        <a className="navbar-brand link-light">
                            LAM
                        </a>
                    </Link>
                    <Button data-bs-toggle="collapse" className="navbar-toggler" data-bs-target="#navcol-1"></Button>
                    <div className="collapse navbar-collapse" id="navcol-1"/>

                    <Link href="/">
                        <a className={ router.pathname === '/' ? "text-gray-500 no-underline " : "text-white no-underline" }>
                            Home
                        </a>
                    </Link>
                    <Link href="proyectos">
                        <a className={ router.pathname === '/proyectos' ? "text-gray-500 no-underline style1 " : "text-white no-underline style1" }>
                            Proyectos
                        </a>
                    </Link>
                    <Link href="nosotros">
                        <a className={ router.pathname === '/nosotros' ? "text-gray-500 no-underline style2" : "text-white no-underline style2" }>
                            Nosotros
                        </a>
                    </Link>
                </Container>
            </Navbar>
            <Carousel variant="dark">
                <Carousel.Item interval={1000}>
                    <img
                        className="w-100 d-block carouselitems" src="https://images3.alphacoders.com/119/1191164.jpg" width="1193" height="384" alt="First slide"
                    />
                    <Carousel.Caption>
                        <h5 className={'text-white font-bold text-3xl'}>Primer slide </h5>
                        <p className={'text-white'}>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="w-100 d-block carouselitems" src="https://www.setreadygame.com/wp-content/uploads/2021/02/Tips-and-tricks-banner.jpg" width="1193" height="384" alt="Second slide"
                    />
                    <Carousel.Caption>
                        <h5 className={'text-white font-bold text-3xl'}>Segundo slide </h5>
                        <p className={'text-white'}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="w-100 d-block carouselitems" src="https://wallpaperaccess.com/full/533313.jpg" width="1193" height="384" alt="Third slide"
                    />
                    <Carousel.Caption>
                        <h5 className={'text-white font-bold text-3xl'}>Tercer slide </h5>
                        <p className={'text-white'}>
                            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
                        </p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        </>
    );
};

export default Header;