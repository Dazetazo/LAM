import React from 'react';
import {Row} from "react-bootstrap";
import Image from "next/image";

const ImgSection = ({imagen, titulo = '', descripcion = '' }) => {
    return (
        <div className="container-fluid p-0">
            <Row className="row g-0 ">
                <div className="flex flex-row justify-between">
                    <div className={'flex flex-col'}>
                        <h2>{titulo}</h2>
                        <p className="lead mb-0 color: rgb(0, 0, 0)"><span>{descripcion}.</span></p>
                    </div>
                    <Image src={imagen} width={500} height={500}/>
                </div>
            </Row>
        </div>
    );
};

export default ImgSection;