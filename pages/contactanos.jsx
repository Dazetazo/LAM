import React from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Layout from "../components/Layout";
import {Container} from "react-bootstrap";

const Contactanos = () => {
    return (
        <Layout>
            <Container>
                <h1 className={'text-5xl text-center mt-16 mb-8'}>Contactanos</h1>
                <div className={'flex flex-col'}>
                    <Form>
                        <Form.Group className="m-3 w-full" controlId="formBasicEmail">
                            <Form.Label>Nombre Completo</Form.Label>
                            <Form.Control type="text" placeholder="Ingresa tu nombre" />
                        </Form.Group>

                        <div className={'flex flex-row justify-between'}>

                            <Form.Group className="m-3 w-full" controlId="formBasicEmail">
                                <Form.Label>Direccion de correo</Form.Label>
                                <Form.Control type="email" placeholder="Ingresa email" />
                            </Form.Group>
                            <Form.Group className="m-3 w-full" controlId="formBasicPassword">
                                <Form.Label>Numero de telefono</Form.Label>
                                <Form.Control type="number" placeholder="Ingresa tu numero de telefono" />
                            </Form.Group>
                        </div>

                        <Form.Group className="m-3 w-full" controlId="formBasicPassword">
                            <Form.Label>Numero de telefono</Form.Label>
                            <Form.Control type="number" placeholder="Ingresa tu numero de telefono" />
                        </Form.Group>
                    </Form>
                </div>
            </Container>
        </Layout>
    );
};

export default Contactanos;