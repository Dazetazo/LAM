import React from 'react';
import Header from "../components/Header";
import Layout from "../components/Layout";
import {Container, Row} from "react-bootstrap";
import Image from "next/image";
import ImgSection from "../components/ImgSection";
import Circulo from "../components/Circulo";

const Home = () => {
    return (
        <>
            <Layout>
                <h1 className="h1project m-20">Últimos proyectos</h1>
                <div className="div1">
                    <Container className="container sombra borderedondo text-center">
                        <Row className="row">
                            <div className="col-md-4">
                                <Image className="imgRedonda imgr" src="/assets/img/Celeste.png" width={220} height={300}/>
                            </div>
                            <div className="col-md-4">
                                <Image className="imgRedonda imgr1" src="/assets/img/Gris.jpg" width={220} height={300}/>
                            </div>
                            <div className="col-md-4">
                                <Image className="imgRedonda imgr2" src="/assets/img/Hollow.jpeg" width={220} height={300}/>
                            </div>
                        </Row>
                    </Container>
                </div>
                <h1 className="h1project">Servicios de Informática</h1>
                <p className="h1project">Brindamos servicios informáticas, entre las cuales destacan:</p>
                <div className="div1">
                    <Container className="container sombra borderedondo text-center">
                        <Row className="row text-center">
                            <div className="col-md-6">
                                <Image className="imgRedonda imgr3" src="/assets/img/pc.png" width={131} height={131}/>
                                <h1 className="text-center text-align: center font-size: 20px">Desarrollo de paginas web</h1>
                            </div>
                            <div className="col-md-6">
                                <Image className="imgRedonda imgr4" src="/assets/img/movil.png" width={131} height={131}/>
                                <h1 className="text-center font-size: 20px">Desarrollo de aplicaciones móviles</h1>
                            </div>
                        </Row>
                    </Container>
                </div>
                <h1 className="h1project">Tecnologías</h1>
                <p className="h1project">Tecnologías utilizadas por nuestros desarrolladores</p>
                <div className="div1">
                    <Container className="container sombra borderedondo text-center">
                        <Row>
                            <div className="col-md-4">
                                <Image className="imgRedonda imgr5" src="/assets/img/React-icon.svg.png" width={100} height={100}/>
                            </div>
                            <div className="col-md-4">
                                <Image className="imgRedonda imgr6" src="/assets/img/JavaScript-Logo.png" width={100} height={100}/>
                            </div>
                            <div className="col-md-4">
                                <Image className="imgRedonda imgr7" src="/assets/img/mongodb.png" width={100} height={100}/>
                            </div>
                        </Row>
                    </Container>
                </div>
                <Container>
                    <section className="showcase m-10">
                        <ImgSection titulo={'Project Violet'} imagen={'/assets/img/Celeste.png'} descripcion={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a mauris nulla. Class aptent taciti sociosqu ad litora torquent per conubia.'}/>
                    </section>
                </Container>
                <Container>
                    <section className="showcase m-10">
                        <ImgSection className="margin-top: 20px" titulo={'Project Janos'} imagen={'/assets/img/Gris.jpg'} descripcion={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a mauris nulla. Class aptent taciti sociosqu ad litora torquent per conubia.'}/>
                    </section>
                </Container>
                <Container>
                    <section className="showcase m-10">
                        <ImgSection titulo={'Project CuadraCulos'} imagen={'/assets/img/Hollow.jpeg'} descripcion={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a mauris nulla. Class aptent taciti sociosqu ad litora torquent per conubia.'}/>
                    </section>
                </Container>
                <h1 className="h1project">¿Por qué nosotros?</h1>
                <p className="h1project">Integer sit amet fringilla tortor. Morbi vel urna lacus. Curabitur eget tortor suscipit, ultricies mi bibendum, pharetra nisi. Mauris ac lorem sit amet ipsum porta sagittis. Vestibulum venenatis mi eget enim pulvinar, in congue quam pulvinar.</p>
                <section className="text-center bg-light testimonials">
                    <Container>
                        <h2 className="m-10">Desarrolladores</h2>
                        <Row>
                            <div className="flex flex-col lg:flex-row">
                                <Circulo imagen={'/Lulos.jpg'} descripcion={'Lukas Flores G.'}/>
                                <Circulo imagen={'/Cuadra.jpg'} descripcion={'Rodrigo Cuadra C.'}/>
                                <Circulo imagen={'/Diego.jpg'} descripcion={'Diego Valenzuela G.'}/>
                            </div>
                        </Row>
                    </Container>
                </section>
            </Layout>
        </>
    );
};

export default Home;
