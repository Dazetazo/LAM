import React from 'react';
import Layout from "../components/Layout";
import { Button, Container } from "react-bootstrap";
import Image from "next/image";
import Circulo from "../components/Circulo";
import Link from "next/link";

const Nosotros = () => {
    return (
        <>
            <Layout>
                <Container>
                    <div className={'flex flex-col lg:flex-row justify-between my-28 grid-cols-2'}>
                        <Image src={'/Compu.png'} width={1500} height={1000}/>
                        <div className={'flex-col mx-20'}>
                            <h1 className={'font-bold'}>QUIENES SOMOS?</h1>
                            <p className={'font-medium text-lg'}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industrys standard dummy text ever since the
                                1500s, when an unknown printer took a galley of type and scrambled it to
                                make a type specimen book.</p>
                            <h2 className={'font-bold'}>EN QUE CONSISTE?</h2>
                            <p className={'font-medium text-lg'}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industrys standard dummy text ever since the
                                1500s, when an unknown printer took a galley of type and scrambled it to
                                make a type specimen book.</p>
                        </div>
                    </div>
                    <div className={'mt-28 flex flex-col'}>
                        <h1 className={'font-bold text-center'}>METODOLOGIA DE TRABAJO</h1>
                        <p className={'font-medium text-lg'}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industrys standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book.</p>
                        <div className={'flex flex-col lg:flex-row'}>
                            <Circulo imagen={'/LAM.jpg'} descripcion={'ANALISIS DE REQUERIMIENTOS'}/>
                            <Circulo imagen={'/LAM.jpg'} descripcion={'PLANIFICACION Y DISENO'}/>
                            <Circulo imagen={'/LAM.jpg'} descripcion={'DESARROLLO DE SOFTWARE'}/>
                            <Circulo imagen={'/LAM.jpg'} descripcion={'PUESTA EN MARCHA'}/>
                        </div>

                        <h1 className={'text-center font-bold mt-20'}>NUESTRO EQUIPO DE TRABAJO</h1>
                        <h3 className={'text-center font-bold mt-10'}>DESARROLLADORES</h3>
                        <div className={'flex flex-col lg:flex-row'}>
                            <Circulo imagen={'/Diego.jpg'} descripcion={'Diego Valenzuela'}/>
                            <Circulo imagen={'/Cuadra.jpg'} descripcion={'Rodrigo Cuadra'}/>
                            <Circulo imagen={'/Lulos.jpg'} descripcion={'Lukas Flores'}/>
                        </div>
                        <h1 className={'text-center font-bold mt-20'}>TE INTERESA COMO TRABAJAMOS?</h1>
                        <Link href={'/contactanos'}>
                            <Button className={'justify-center items-center mx-auto my-10 h-16 w-80 border'}>
                                Contactanos
                            </Button>
                        </Link>
                    </div>
                </Container>
            </Layout>
        </>
    );
};

export default Nosotros;