import React from 'react';
import Layout from "../components/Layout";
import {Container} from "react-bootstrap";
import ImgSection from "../components/ImgSection";
import {BsGithub, BsInstagram} from "react-icons/bs";
import Link from "next/link";

const Proyectos = () => {
    return (
        <>
            <Layout>
                <h1 className="h1project m-20">Nuestros Proyectos</h1>
                <Container>
                    <section className="showcase m-10">
                        <ImgSection titulo={'Project Violet'} imagen={'/assets/img/Celeste.png'} descripcion={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a mauris nulla. Class aptent taciti sociosqu ad litora torquent per conubia.'}/>.
                        <div className={'flex items-center justify-center md:mt-0 mt-10'}>
                            <a href="#">
                                <BsGithub className={'text-gray-500 text-4xl m-2'}/>
                            </a>
                            <a href="#">
                            <BsInstagram className={'text-gray-500 text-4xl m-2'}/>
                            </a>
                        </div>
                    </section>
                </Container>
                <Container>
                    <section className="showcase m-10">
                        <ImgSection className="margin-top: 20px" titulo={'Project Janos'} imagen={'/assets/img/Gris.jpg'} descripcion={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a mauris nulla. Class aptent taciti sociosqu ad litora torquent per conubia.'}/>
                        <div className={'flex items-center justify-center md:mt-0 mt-10'}>
                            <a href="#">
                                <BsGithub className={'text-gray-500 text-4xl m-2'}/>
                            </a>
                            <a href="#">
                                <BsInstagram className={'text-gray-500 text-4xl m-2'}/>
                            </a>
                        </div>
                    </section>
                </Container>
                <Container>
                    <section className="showcase m-10">
                        <ImgSection titulo={'Project CuadraCulos'} imagen={'/assets/img/Hollow.jpeg'} descripcion={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a mauris nulla. Class aptent taciti sociosqu ad litora torquent per conubia.'}/>
                        <div className={'flex items-center justify-center md:mt-0 mt-10'}>
                            <a href="#">
                                <BsGithub className={'text-gray-500 text-4xl m-2'}/>
                            </a>
                            <a href="#">
                                <BsInstagram className={'text-gray-500 text-4xl m-2'}/>
                            </a>
                        </div>
                    </section>
                </Container>
                <Container>
                <section className="showcase m-10">
                    <ImgSection titulo={'Project Violet'} imagen={'/assets/img/Celeste.png'} descripcion={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a mauris nulla. Class aptent taciti sociosqu ad litora torquent per conubia.'}/>
                    <div className={'flex items-center justify-center md:mt-0 mt-10'}>
                        <a href="#">
                            <BsGithub className={'text-gray-500 text-4xl m-2'}/>
                        </a>
                        <a href="#">
                            <BsInstagram className={'text-gray-500 text-4xl m-2'}/>
                        </a>
                    </div>
                </section>
            </Container>
                <Container>
                    <section className="showcase m-10">
                        <ImgSection className="margin-top: 20px" titulo={'Project Janos'} imagen={'/assets/img/Gris.jpg'} descripcion={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a mauris nulla. Class aptent taciti sociosqu ad litora torquent per conubia.'}/>
                        <div className={'flex items-center justify-center md:mt-0 mt-10'}>
                            <a href="#">
                                <BsGithub className={'text-gray-500 text-4xl m-2'}/>
                            </a>
                            <a href="#">
                                <BsInstagram className={'text-gray-500 text-4xl m-2'}/>
                            </a>
                        </div>
                    </section>
                </Container>
                <Container>
                    <section className="showcase m-10">
                        <ImgSection titulo={'Project CuadraCulos'} imagen={'/assets/img/Hollow.jpeg'} descripcion={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a mauris nulla. Class aptent taciti sociosqu ad litora torquent per conubia.'}/>
                        <div className={'flex items-center justify-center md:mt-0 mt-10'}>
                            <a href="#">
                                <BsGithub className={'text-gray-500 text-4xl m-2'}/>
                            </a>
                            <a href="#">
                                <BsInstagram className={'text-gray-500 text-4xl m-2'}/>
                            </a>
                        </div>
                    </section>
                </Container>
            </Layout>
        </>
    );
};

export default Proyectos;